********************************************************************
* Boot - 65SPI Boot module for MOOH and Spinx-512 boards
*        with SDHC card.
* Provides HWInit, HWTerm, HWRead which are called by code in
* "use"d boot_common.asm
*
* $Id$
*
* Edt/Rev  YYYY/MM/DD  Modified by
* Comment
* ------------------------------------------------------------------
*          2018/02/28  Tormod Volden
* Created.

               nam   Boot
               ttl   65SPI Boot module

               IFP1
                use  defsfile
               ENDC

* Common booter-required defines
LSN24BIT       equ       1
FLOPPY         equ       0

* Port offsets
SPIDATA        equ       0
SPICTRL        equ       1                  write-only
SPISTATUS      equ       SPICTRL            read-only
SPICLK         equ       2
SPISIE         equ       3

SPICS          equ       1                  hardcode SEL0 for now

* NOTE: these are U-stack offsets, not DP
seglist        rmb       2                  pointer to segment list
blockloc       rmb       2                  pointer to memory requested
blockimg       rmb       2                  duplicate of the above
bootsize       rmb       2                  size in bytes
LSN0Ptr        rmb       2                  in-memory LSN0 pointer
size           equ       .


tylg           set       Systm+Objct
atrv           set       ReEnt+rev
rev            set       $00
edition        set       1

               mod  eom,name,tylg,atrv,start,size

name           fcs       /Boot/
               fcb       edition


*--------------------------------------------------------------------------
* HWInit - Initialize the device
*
*    Entry:
*       Y  = hardware address
*
*    Exit:
*       Carry Clear = OK, Set = Error
*       B  = error (Carry Set)
*
HWInit
	* Partion table could be detected here
	* and partition offset determined
	* We assume the bootloader has set up the SD card
	clrb
	rts

*--------------------------------------------------------------------------
* HWTerm - Terminate the device
*
*    Entry:
*       Y  = hardware address
*
*    Exit:
*       Carry Clear = OK, Set = Error
*       B = error (Carry Set)
*
HWTerm         clrb                         no error
               rts


***************************************************************************
     use  boot_common.asm
***************************************************************************

*
* HWRead - Read a 256 byte sector from the device
*
*    Entry:
*       Y  = hardware address
*       B  = bits 23-16 of LSN
*       X  = bits 15-0  of LSN
*       blockloc,u = where to load the 256 byte sector
*
*    Exit:
*       X  = ptr to data (i.e. ptr in blockloc,u)
*       Carry Clear = OK, Set = Error
*
HWRead
	lsrb			; LSN (256) -> LBA (512)
	pshs	b
	tfr	x,d
	rora
	rorb
	pshs cc			; carry means odd LSN

	; add partition offset
	addd	OffsLSW,pcr	; LBA offset low bytes
	pshs	d
	ldd	OffsMSW,pcr	; LBA offset high bytes
	adcb	3,s
	adca	#0
	pshs	d		; 4 byte LBA on stack (+ cc, lba-msb)

	; request block
	lda	#8
	sta	5,s		; reuse as counter
	ldb	#$40+17		; CMD17 (Read Single Block)
	bsr	_sd_send_command
	tstb
	bne	failblk
	clrb
	bsr	_sd_spi_wait
	subb	#$FE
	bne	failblk

	lda	#$14		; 65SPI: FRX on, ext clock
	sta	SPICTRL,y
	lda	SPIDATA,y	; start shifting in first byte

	; which half sector to read to buffer
	lda	4,s	; saved cc
	lsra		; saved carry into carry
	bcc	read2buf

	; ignore first 256 bytes if odd LSN
	clrb
spib1	lda	SPIDATA,y
	decb
	bne	spib1

read2buf
	; read 256 bytes into buffer
	ldx	blockloc,u
	clrb
spib2	lda	SPIDATA,y
	sta	,x+
	decb
	bne	spib2

	lda	4,s		; saved cc
	lsra			; saved carry into carry
	bcs	doneblk

	; ignore last 256 bytes if even LSN
	; we could interrupt block transfer instead
	clrb
spib3	lda	SPIDATA,y
	decb
	bne	spib3

doneblk
	clrb			; clear carry
ret
	pshs	cc
	lda	#$04		; FRX off, ext clock
	sta	SPICTRL,y

	leax	-256,x
	bsr	_sd_spi_release	; clobbers A,B,CC
	puls	cc
	leas	6,s
	rts
failblk	comb			; set carry
	bra	ret

* SD card routines rewritten from FUZIX C code

* clobbers A,B
_sd_spi_release
	bsr _sd_spi_raise_cs
	bra _sd_spi_receive_byte

* want_ff in B, returns status in B
* clobbers A,X
* TODO maybe more efficient to have two functions
_sd_spi_wait
	ldx #500
	tstb
	beq noff
waitl	bsr _sd_spi_receive_byte	; clobbers A,B
	cmpb #$FF
	beq don1
	leax -1,x
	bne waitl
don1	rts

noff	bsr _sd_spi_receive_byte	; clobbers A,B
	cmpb #$FF
	bne don1
	leax -1,x
	bne noff
	bra don1

* cmd in B, arg on stack, returns status in B
* clobbers A,X

_sd_send_command
	* simplified, no ACMD etc
	pshs b
	bsr _sd_spi_release		; clobbers A,B
	bsr _sd_spi_lower_cs		; clobbers A
	ldb #1
	bsr _sd_spi_wait		; clobbers A,B,X
	cmpb #$FF
	beq wok
	ldb #$FF
	puls a,pc			; restore stack
wok
	puls b
	bsr _sd_spi_transmit_byte	; send command index, clobbers A
	ldb 2,s
	bsr _sd_spi_transmit_byte	; arg[31..24], clobbers A
	ldb 3,s
	bsr _sd_spi_transmit_byte	; arg[23..16]
	ldb 4,s
	bsr _sd_spi_transmit_byte	; arg[15..8]
	ldb 5,s
	bsr _sd_spi_transmit_byte	; arg[7..0]

	ldb #1
	* simplified, no CMD0 or CMD8 etc
	bsr _sd_spi_transmit_byte	; CRC
	ldx #20				; command response timeout
waitresp
	bsr _sd_spi_receive_byte
	tstb
	bpl valresp
	leax -1,x
	bne waitresp
valresp rts

_sd_spi_raise_cs:
	lda SPISIE,y
	ora #SPICS
	sta SPISIE,y
	rts

_sd_spi_lower_cs:
	lda SPISIE,y
	anda #$FF-SPICS
	sta SPISIE,y
	rts

_sd_spi_transmit_byte:
	stb SPIDATA,y
txwait:
	lda SPISTATUS,y
	anda #$20		; BSY
	bne txwait
	rts

_sd_spi_receive_byte:
	lda #$FF
	sta SPIDATA,y
rxwait:
	lda SPISTATUS,y		; wait for bit 7 to set
	bpl rxwait
	ldb SPIDATA,y
	rts

*--------------------------------------------------------------------------

     IFGT Level-1
* L2 kernel file is composed of rel, boot, krn. The size of each of these
* is controlled with filler, so that (after relocation):
* rel  starts at $ED00 and is $130 bytes in size
* boot starts at $EE30 and is $1D0 bytes in size
* krn  starts at $F000 and ends at $FEFF (there is no 'emod' at the end
*      of krn and so there are no module-end boilerplate bytes)
*
* Filler to get to a total size of $1D0. 3, 2, 1 represent bytes after
* the filler: the end boilerplate for the module, fdb and fcb respectively.
Filler         fill      $39,$1D0-3-4-2-1-*
     ENDC

WhichDrv       fcb       0		; BootDr
Address        fdb       $FF6C		; 65SPI interface on MOOH/Spinx-512
OffsMSW        fdb       $0000		; LBA offset to NitrOS-9 partition
OffsLSW        fdb       $0200

               emod
eom            equ       *
               end
